/* create command : anvas-sketch webgl.js --new --template=three */
const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random');
const palettes = require('nice-color-palettes');
const eases = require('eases');
const BezierEasing = require('bezier-easing');

// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require('three');

require('three/examples/js/controls/OrbitControls');

const settings = {
  dimensions: [512, 512],
  fps: 30,
  duration: 4, 
  animate: true,
  context: 'webgl',
  attributes: { antialias: true }
};

const sketch = ({ context }) => {
  const renderer = new THREE.WebGLRenderer({
    context
  });

  renderer.setClearColor('hsl(0, 0%, 95%)', 1);

  const camera = new THREE.OrthographicCamera();

  const scene = new THREE.Scene();
  const box = new THREE.BoxGeometry(1, 1, 1);
  const palette = random.pick(palettes);

  for(let i = 0; i < 40; i++){
  const mesh = new THREE.Mesh(
      box,    
      new THREE.MeshStandardMaterial({
      color: random.pick(palette)
    })
  );
  mesh.scale.set(
    random.range(-1, 1), 
    random.range(-1, 1), 
    random.range(-1, 1)
  );
  mesh.position.set(
    random.range(-1, 1), 
    random.range(-1, 1), 
    random.range(-1, 1)
  )
  mesh.scale.multiplyScalar(0.5);
  scene.add(mesh);
  }

  scene.add(new THREE.AmbientLight('white'));
  const light = new THREE.DirectionalLight('white', 1);
        light.position.set(2,2,4);
        scene.add(light);

  //https://cubic-bezier.com/#.75,.01,.37,1.23
  const easeFn = BezierEasing(0.75,0.01,0.37,1.23);

  return {
    resize ({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
    
      const aspect = viewportWidth / viewportHeight;
      const zoom = 2.0;

      camera.left = -zoom * aspect;
      camera.right = zoom * aspect;
      camera.top = zoom;
      camera.bottom = -zoom;

      camera.near = -100;
      camera.far = 100;

      camera.near = -100;
      camera.far = 100;

      camera.position.set(zoom, zoom, zoom);
      camera.lookAt(new THREE.Vector3());

      camera.updateProjectionMatrix();
    },
    render ({ playhead }) {
      // 'pleahead' only used when there is a duration
      // if no duration use 'time'
      const t =  Math.sin(playhead * Math.PI);
      scene.rotation.y = easeFn(t);
      renderer.render(scene, camera);
    },
    unload () {
      renderer.dispose();
    }
  };
};

canvasSketch(sketch, settings);
